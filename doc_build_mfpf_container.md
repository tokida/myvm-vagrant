# IBM MobileFirstPlatformFundation Container Build

* コンテナのビルド方法のメモ
* https://developer.ibm.com/mobilefirstplatform/documentation/getting-started-7-1/bluemix/run-foundation-on-bluemix/
* 上記のURLの内容に従い実施することでビルド出来ます。


## ファイルのダウロード

以下のファイルをダウンロードし、適当なディレクトリで展開します。

```
curl -O http://public.dhe.ibm.com/ibmdl/export/pub/software/products/en/MobileFirstPlatform/mfpfcontainers/ibm-mfpf-container-7.1.0.0-eval.zip
```

## カスタマイズ

先ほどのドキュメント内の "Step 3: Using the configuration files" に従い設定を行います。

Group Containerを利用する場合には、`startservr.sh`ではなく`startservergroup.sh` を修正します。


## Buildの実行

以下の作業を root ユーザで実行します

```
cd ibm-mfpf-container-7.1.0.0-eval/mfpf-server/server
./installcontainercli.sh
./initenv.sh args/initenv.properties
./prepareserverdbs.sh args/prepareserverdbs.properties
./prepareserver.sh args/prepareserver.properties
```

## 実行

```
./startserver.sh args/startserver.properties
```
