# はじめに

IBM Cloud (SoftLayer/Bluemix）等を利用する上で必要になるアプリケーション/ClientをVagrant上に構築するためのスクリプトです。

以下のクライアントが導入されます。

* Ansible
* SoftLayer Cli (slcli)
* Bluemix Cli (cf)
* Bluemix Cli IBM Container Plugin (cf ic)
* Bluemix IBM Container Cli (ice)
* Docker
* IBM Mobile First Platform Fundation Cli (mpf)
* java7 for mpf

注意：すごく時間がかかります。

# 導入方法

##  Vagrantの実行について

このツールは ubuntu 15.04 をベースにしているためVagrantのBoxを以下のコマンドでダウンロードして下さい

1. Vagrantを導入する
1. Boxイメージをダウンロード
```
vagrant box add ubuntu15 https://github.com/kraksoft/vagrant-box-ubuntu/releases/download/15.04/ubuntu-15.04-amd64.box
```

1. Vagrant環境の初期化
```
git clone https://bitbucket.org/tokida/myvm-vagrant.git
vagrant plugin install vagrant-vbguest
vagrant up
vagrant provision
vagrant ssh
```
1. カレントディレクトリに作成される`Vagrantfile`を確認する
1. 必要に応じてローカルのフォルダを仮想内から参照できるようにしておく
1. `vagrant provision`で初期アプリケーションの構成
1. `vagrant up`で仮想環境を起動
1. `vagrant ssh`でログイン
1. 必要なモジュールを導入



## 各種ツールの導入

### VagrantからAnsible/ServerSpecの導入

ツール群はAnsibleを利用して導入するためにVagrantの機能を利用して導入します。

```
> vagrant provision
```

このツールをVagrantで利用しない場合には以下のAnsibleを該当のサーバにて実行して下さい。hosts等を適切に記載すればRemoteサーバでも利用できると思います。

Ubuntu 15.04 においてAnsibleは以下のコードで導入可能です

```
sudo apt-get install -y software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install -y ansible
```



### 個別の導入

個別に導入するために Vagrantで作られた仮想サーバにログインして導入をします。
必要に応じて選択が出来るように自動で全ては導入しないようにしています。


```
> vagrant ssh
$ cd /vagrant/ansible
$ sudo ansible-playbook -t <tag> site.yml
```

利用できるタグ<tag>は以下のとおりです。

|tag|tools name|
|:--|:--|
|common | 最初に実行しておいて下さい |
|softlayer | SoftLayer Cli|
|bluemix | Bluemix Cli |
|bluemix_ic | Bluemix Plugin IBM Container |
|ice  | Bluemix IBM Container Cli |
|mpf  | IBM Mobile First Platform Fundation Cli |
|docker | docker engine |

* mfpは導入時にJava7が導入されます。
* 導入には非常に時間がかかります。特にMFPはダウンロードサイズも大きいため非常に時間がかかります。
* Dockerは、IBM Container関連を実行する際には必要になります。
